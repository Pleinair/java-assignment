Local csv folder: JavaAssignment\bin\main\static\csv

This assignment is made using Spring Boot, the structure is as follows:
1. The entities represent entities in the database in the form of a Java class
2. The models is similar in structure to entities but are not persisted into the database
3. The repositories are interfaces for database queries
4. The services use the repositories and provide callable functions
5. The controllers are api's to be called through HTTP requests that connect to the services (REST API)

The loader Daemon is located in the 'daemon' and automatically watches the local csv folder for changes after the application boots.

The mySQL tables in the database would not be automatically created, therefore the tables have to be created beforehand.
Connection to the mySQL server is configured in the application.yml under resources (src/main/resources).
The sql structure for the tables are as follows:

1. Team
table: team
columns: TEAM_ID

2. Team-Skill
table: team_skill
columns: id(had to include because of spring requirement), TEAM_ID, SKILL

3. Task
table: task
columns: TASK_ID, SKILL

4. Assignment (assignment of teams to tasks based on skills)
table: assignment
columns: TEAM_ID, SKILL_ID_LIST, TASK_ID_LIST

all of the fields are in varchar(255)/String with the exception of id which is an int

