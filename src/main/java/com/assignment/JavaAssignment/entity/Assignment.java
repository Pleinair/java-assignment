package com.assignment.JavaAssignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "assignment")
public class Assignment {

	@Id
	@Column(name = "TEAM_ID")
	private String teamId;
	
	@Column(name = "SKILL_ID_LIST")
	private String skillList;
	
	@Column(name = "TASK_ID_LIST")
	private String taskList;
	
	public Assignment() {
		super();
	}
	
	@JsonCreator
	public Assignment(@JsonProperty("teamId") String teamId, @JsonProperty("skillList") String skillList,
			@JsonProperty("taskList") String taskList) {
		super();
		this.teamId = teamId;
		this.skillList = skillList;
		this.taskList = taskList;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getSkillList() {
		return skillList;
	}

	public void setSkillList(String skillList) {
		this.skillList = skillList;
	}

	public String getTaskList() {
		return taskList;
	}

	public void setTaskList(String taskList) {
		this.taskList = taskList;
	}
}
