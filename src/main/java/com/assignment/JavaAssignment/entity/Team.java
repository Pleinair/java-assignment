package com.assignment.JavaAssignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "team", schema = "public")
public class Team {
	@Id
	@Column(name = "TEAM_ID")
	private String teamId;
	
	public Team() {
		super();
	}
	
	@JsonCreator
	public Team(@JsonProperty("teamId") String teamId) {
		super();
		this.teamId = teamId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
}
