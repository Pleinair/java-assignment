package com.assignment.JavaAssignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "task", schema = "public")
public class Task {
	
	@Id
	@Column(name = "TASK_ID")
	private String taskId;
	
	@Column(name = "SKILL")
	private String skill;
	
	public Task() {
		super();
	}
	
	@JsonCreator
	public Task(@JsonProperty("taskId") String taskId, @JsonProperty("skill") String skill) {
		super();
		this.taskId = taskId;
		this.skill = skill;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}
}
