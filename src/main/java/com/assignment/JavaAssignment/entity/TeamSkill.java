package com.assignment.JavaAssignment.entity;

import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "team_skill", schema = "public")
public class TeamSkill {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id = (int)(1000.0*Math.random());	
	
	@Column(name = "TEAM_ID", unique = false)
	private String teamId;
	
	@Column(name = "SKILL", unique = false)
	private String skill;
	
	public TeamSkill() {
		super();
	}
	
	@JsonCreator
	public TeamSkill(@JsonProperty("id") int id, @JsonProperty("teamId") String teamId, @JsonProperty("skill") String skill) {
		super();
		this.id = id;
		this.teamId = teamId;
		this.skill = skill;
	}
	
	@JsonCreator
	public TeamSkill(@JsonProperty("teamId") String teamId, @JsonProperty("skill") String skill) {
		super();
		this.teamId = teamId;
		this.skill = skill;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
