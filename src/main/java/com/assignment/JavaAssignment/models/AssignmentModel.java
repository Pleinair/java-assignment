package com.assignment.JavaAssignment.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class AssignmentModel {

	private String teamId;
	private List<String> skills = new ArrayList<>();
	private List<String> tasks = new ArrayList<>();
	
	public AssignmentModel() {
		super();
	}
	
	@JsonCreator
	public AssignmentModel(@JsonProperty("teamId") String teamId, @JsonProperty("skills") List<String> skills,
			@JsonProperty("tasks") List<String> tasks) {
		super();
		this.teamId = teamId;
		this.skills = skills;
		this.tasks = tasks;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public List<String> getTasks() {
		return tasks;
	}

	public void setTasks(List<String> tasks) {
		this.tasks = tasks;
	}
}
