package com.assignment.JavaAssignment;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.assignment.JavaAssignment.daemon.DaemonThread;

@SpringBootApplication
public class JavaAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaAssignmentApplication.class, args);
		
		try {
			// workaround to connect spring structure to daemon
		    URL url = new URL("http://localhost:8081/start");
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("GET");
		    InputStream in = conn.getInputStream();
	    }
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
