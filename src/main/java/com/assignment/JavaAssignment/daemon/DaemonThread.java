package com.assignment.JavaAssignment.daemon;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.file.StandardWatchEventKinds.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.assignment.JavaAssignment.entity.Task;
import com.assignment.JavaAssignment.entity.Team;
import com.assignment.JavaAssignment.entity.TeamSkill;
import com.assignment.JavaAssignment.services.TaskService;
import com.assignment.JavaAssignment.services.TeamService;
import com.assignment.JavaAssignment.services.TeamSkillService;

public class DaemonThread extends Thread{
	private Path path;
	private TeamService teamService;
	private TaskService taskService;
	private TeamSkillService tsService;
	
	public DaemonThread() {
		
	}
	
	@Autowired
	public DaemonThread(TeamService teamService, TaskService taskService, TeamSkillService tsService) {
		super();
		
		this.teamService = teamService;
		this.taskService = taskService;
		this.tsService = tsService;
		
		File file = new File("");
		this.setDaemon(true);
		try {
			file = new ClassPathResource("static/csv").getFile();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		path = file.toPath();
	}
	
	public DaemonThread(Path path) {
		super();
		this.setDaemon(true);
		this.path = path;
	}
	
	public void run() {
		System.out.println("Check if its DaemonThread: " 
                + Thread.currentThread().isDaemon());
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(path,
                    "basic:isDirectory");
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + path
                        + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        System.out.println("Watching path: " + path);

        // We obtain the file system of the Path
        FileSystem fs = path.getFileSystem();

        // We create the new WatchService using the new try() block
        try (WatchService service = fs.newWatchService()) {

            // We register the path to the service
            // We watch for creation events
        	path.register(service, 
        			ENTRY_CREATE, 
        			ENTRY_MODIFY, 
        			ENTRY_DELETE);

            // Start the infinite polling loop
            // WatchKey key = null;
            for(;;) {
                final WatchKey key = service.take();

                Kind<?> kind = null;
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    // Get the type of the event
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                        continue; // loop
                    } else if (ENTRY_CREATE == kind) {
                        // A new Path was created
                        @SuppressWarnings("unchecked")
						Path newPath = ((WatchEvent<Path>) watchEvent)
                                .context();
                        // Output
                        File newFile = new ClassPathResource("static/csv/" + newPath).getFile();
                        try(Scanner scanner = new Scanner(newFile)){
                        	int i = 0;
                        	List<Task> tasks = new ArrayList<>();
                        	List<Team> teams = new ArrayList<>();
                        	List<TeamSkill> teamSkills = new ArrayList<>();
                        	while(scanner.hasNextLine()) {
                        		String line = scanner.nextLine();
                        		line = line.replaceAll("\\\"", "");
                        		line = line.replaceAll("\\\\", "");
                        		String[] items = null;
                        		if(!newPath.toString().equals("team.csv")) {
                        			items = line.split(",");
                        		}
                        		if(i == 0) {
                        			i++;
                        			continue;
                        		}
                        		else {
                        			switch(newPath.toString()) {
	                        			case "task.csv":
	                        				Task task = new Task(items[0], items[1]);
	                        				tasks.add(task);
	                        				break;
	                        			
	                        			case "team.csv":
	                        				Team team = new Team(line);
	                        				teams.add(team);
	                        				break;
	                        				
	                        			case "team_skill.csv":
	                        				TeamSkill ts = new TeamSkill(items[0], items[1]);
	                        				teamSkills.add(ts);
	                        				break;
	                        				
	                        			default:
	                        				System.out.println("Failed to process: " + newPath.toString());
	                        			
                        			}
                        		}
                        	}
                        	
                        	if(tasks.size() > 0) {
                        		taskService.saveAll(tasks);
                        	}
                        	if(teams.size() > 0) {
                        		teamService.saveAll(teams);
                        	}
                        	if(teamSkills.size() > 0) {
                        		tsService.saveAll(teamSkills);
                        	}
                        	
                        	scanner.close();
                        }
                        
                        System.out.println("New path created: " + newPath);
                        
                    	if(newFile.delete()) {
                    		System.out.println("File deleted successfully");
                    	}
                    	else {
                    		System.out.println("Failed to delete file");
                    	}
                    } else if (ENTRY_MODIFY == kind) {
                        // modified
                        @SuppressWarnings("unchecked")
						Path newPath = ((WatchEvent<Path>) watchEvent)
                                .context();
                        // Output
                        System.out.println("New path modified: " + newPath);
                    }
                }

                if (!key.reset()) {
                	System.out.println("Break");
                    break; // loop
                }
            }

        } catch (IOException ioe) {
        	System.out.println("IOE");
            ioe.printStackTrace();
        } catch (InterruptedException ie) {
        	System.out.println("Interrupted");
            ie.printStackTrace();
        }
	}
}
