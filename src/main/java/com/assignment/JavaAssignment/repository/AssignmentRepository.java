package com.assignment.JavaAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.JavaAssignment.entity.Assignment;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, String>{

}
