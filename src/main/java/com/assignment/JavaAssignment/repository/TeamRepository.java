package com.assignment.JavaAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.JavaAssignment.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, String>{

}
