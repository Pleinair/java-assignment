package com.assignment.JavaAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.JavaAssignment.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String>{

}
