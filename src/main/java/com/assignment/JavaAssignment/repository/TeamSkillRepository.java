package com.assignment.JavaAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.JavaAssignment.entity.TeamSkill;

@Repository
public interface TeamSkillRepository extends JpaRepository<TeamSkill, Integer>{

}
