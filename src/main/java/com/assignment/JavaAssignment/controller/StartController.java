package com.assignment.JavaAssignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.JavaAssignment.daemon.DaemonThread;
import com.assignment.JavaAssignment.services.TaskService;
import com.assignment.JavaAssignment.services.TeamService;
import com.assignment.JavaAssignment.services.TeamSkillService;

@RestController
@CrossOrigin
@RequestMapping("/start")
public class StartController {

	private TaskService taskService;
	private TeamService teamService;
	private TeamSkillService tsService;
	
	@Autowired
	public StartController(TaskService taskService, TeamService teamService, TeamSkillService tsService) {
		super();
		this.taskService = taskService;
		this.teamService = teamService;
		this.tsService = tsService;
	}
	
	@GetMapping("")
	public void Start() {
		DaemonThread t1 = new DaemonThread(teamService, taskService, tsService);
		t1.start();
	}
}
