package com.assignment.JavaAssignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.JavaAssignment.entity.Task;
import com.assignment.JavaAssignment.services.TaskService;

@RestController
@CrossOrigin
@RequestMapping("/api/task")
public class TaskController {

	private TaskService taskService;
	
	@Autowired
	public TaskController(TaskService taskService) {
		super();
		this.taskService = taskService;
	}
	
	@GetMapping("/getTaskList")
	public List<Task> getTaskList(){
		return taskService.findAll();
	}
	
	@GetMapping("/getTask")
	public Task getTaskById(@RequestParam String id) {
		return taskService.findById(id).orElse(null);
	}
	
	@PostMapping("/insertTask")
	public Task insertTask(@RequestBody Task task) {
		return taskService.save(task);
	}
	
	@PostMapping("/insertTaskList")
	public List<Task> insertTaskList(@RequestBody List<Task> tasks){
		return taskService.saveAll(tasks);
	}
	
	@DeleteMapping("/deleteTask")
	public String deleteTask(@RequestParam String id) {
		taskService.deleteById(id);
		return "Deleted task with id: " + id;
	}
	
	@DeleteMapping("/deleteAllTasks")
	public String deleteAllTasks() {
		taskService.deleteAll();
		return "Deleted all tasks";
	}
}
