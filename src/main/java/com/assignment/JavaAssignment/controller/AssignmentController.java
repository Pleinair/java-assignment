package com.assignment.JavaAssignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.JavaAssignment.entity.Assignment;
import com.assignment.JavaAssignment.models.AssignmentModel;
import com.assignment.JavaAssignment.services.AssignmentService;

@RestController
@CrossOrigin
@RequestMapping("/api/assignment")
public class AssignmentController {

	private AssignmentService asService;
	
	@Autowired
	public AssignmentController(AssignmentService asService) {
		super();
		this.asService = asService;
	}
	
	@GetMapping("/assignTasks")
	public List<Assignment> assignTasks(){
		return asService.assignTasks();
	}
	
	@GetMapping("/getAssignmenList")
	public List<Assignment> getAssignment(){
		return asService.findAll();
	}
	
	@GetMapping("/getAssignment")
	public Assignment getAssignmentById(String id) {
		return asService.findById(id).orElse(null);
	}
	
	@DeleteMapping("/deleteAllAssignments")
	public String deleteAllAssignments() {
		asService.deleteAll();
		return "Deleted all assignments";
	}
}
