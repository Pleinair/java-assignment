package com.assignment.JavaAssignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.JavaAssignment.entity.TeamSkill;
import com.assignment.JavaAssignment.services.TeamSkillService;

@RestController
@CrossOrigin
@RequestMapping("/api/teamSkill")
public class TeamSkillController {

	private TeamSkillService tsService;
	
	@Autowired
	public TeamSkillController(TeamSkillService tsService) {
		super();
		this.tsService = tsService;
	}
	
	@GetMapping("/getTeamSkillList")
	public List<TeamSkill> getTeamSkillList(){
		return tsService.findAll();
	}
	
	@GetMapping("/getTeamSkill")
	public TeamSkill getTeamSkillById(@RequestParam int id) {
		return tsService.findById(id).orElse(null);
	}
	
	@PostMapping("/insertTeamSkill")
	public TeamSkill insertTeamSkill(@RequestBody TeamSkill teamSkill) {
		return tsService.save(teamSkill);
	}
	
	@DeleteMapping("/deleteTeamSkill")
	public String deleteTeamSkill(@RequestParam int id) {
		tsService.deleteById(id);
		return "Deleted team-skill with id: " + id;
	}
	
	@DeleteMapping("/deleteAllTeamSkills")
	public String deleteAllTeamSkills() {
		tsService.deleteAll();
		return "Deleted all team-skills";
	}
}
