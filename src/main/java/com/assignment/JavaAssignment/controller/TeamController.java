package com.assignment.JavaAssignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.JavaAssignment.entity.Team;
import com.assignment.JavaAssignment.services.TeamService;

@RestController
@CrossOrigin
@RequestMapping("/api/team")
public class TeamController {
	private TeamService teamService;
	
	@Autowired
	public TeamController(TeamService teamService) {
		super();
		this.teamService = teamService;
	}
	
	@GetMapping("/getTeamList")
	public List<Team> getTeamList(){
		return teamService.findAll();
	}
	
	@GetMapping("/getTeam")
	public Team getTeamById(@RequestParam String id){
		return teamService.findById(id).orElse(null);
	}
	
	@PostMapping("/insertTeam")
	public Team insertTeam(@RequestBody Team team) {
		return teamService.save(team);
	}
	
	@PostMapping("/insertTeamList")
	public List<Team> insertTeamList(@RequestBody List<Team> teams){
		return teamService.saveAll(teams);
	}
	
	@DeleteMapping("/deleteTeam")
	public String delete(@RequestParam String id) {
		teamService.deleteById(id);
		return "Deleted team with id: " + id;
	}
	
	@DeleteMapping("/deleteAllTeams")
	public String deleteAll() {
		teamService.deleteAll();
		return "Deleted all teams";
	}
}
