package com.assignment.JavaAssignment.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.JavaAssignment.entity.Task;
import com.assignment.JavaAssignment.repository.TaskRepository;

@Service
public class TaskService {

	private TaskRepository taskRepo;
	
	@Autowired
	public TaskService(TaskRepository taskRepo) {
		this.taskRepo = taskRepo;
	}
	
	// GET methods
	public List<Task> findAll(){
		return taskRepo.findAll();
	}
	
	public Optional<Task> findById(String id) {
		return taskRepo.findById(id);
	}
	
	// INSERT methods
	public Task save(Task task) {
		return taskRepo.save(task);
	}
	
	public List<Task> saveAll(List<Task> tasks){
		return taskRepo.saveAll(tasks);
	}
	
	// DELETE methods
	public void delete(Task task) {
		taskRepo.delete(task);
	}
	
	public void deleteById(String id) {
		taskRepo.deleteById(id);
	}
	
	public void deleteAll() {
		taskRepo.deleteAll();
	}
}
