package com.assignment.JavaAssignment.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.JavaAssignment.entity.TeamSkill;
import com.assignment.JavaAssignment.repository.TeamSkillRepository;

@Service
public class TeamSkillService {

	private TeamSkillRepository teamSkillRepo;
	
	@Autowired
	public TeamSkillService(TeamSkillRepository teamSkillRepo) {
		this.teamSkillRepo = teamSkillRepo;
	}
	
	// GET methods
	public List<TeamSkill> findAll(){
		return teamSkillRepo.findAll();
	}
	
	public Optional<TeamSkill> findById(int id){
		return teamSkillRepo.findById(id);
	}
	
	// INSERT methods
	public TeamSkill save(TeamSkill teamSkill) {
		return teamSkillRepo.save(teamSkill);
	}
	
	public List<TeamSkill> saveAll(List<TeamSkill> teamSkills){
		return teamSkillRepo.saveAll(teamSkills);
	}
	
	// DELETE methods
	public void delete(TeamSkill teamSkill) {
		teamSkillRepo.delete(teamSkill);
	}
	
	public void deleteById(int id) {
		teamSkillRepo.deleteById(id);
	}
	
	public void deleteAll() {
		teamSkillRepo.deleteAll();
	}
}
