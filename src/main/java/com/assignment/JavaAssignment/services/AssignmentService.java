package com.assignment.JavaAssignment.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.JavaAssignment.entity.Assignment;
import com.assignment.JavaAssignment.entity.Task;
import com.assignment.JavaAssignment.entity.Team;
import com.assignment.JavaAssignment.entity.TeamSkill;
import com.assignment.JavaAssignment.models.AssignmentModel;
import com.assignment.JavaAssignment.repository.AssignmentRepository;

@Service
public class AssignmentService {

	private TaskService taskService;
	private TeamService teamService;
	private TeamSkillService tsService;
	private AssignmentRepository asRepo;
	
	@Autowired
	public AssignmentService(TaskService taskService, TeamService teamService, TeamSkillService tsService,
			AssignmentRepository asRepo) {
		super();
		this.taskService = taskService;
		this.teamService = teamService;
		this.tsService = tsService;
		this.asRepo = asRepo;
	}
	
	// GET methods
	public List<Assignment> findAll(){
		return asRepo.findAll();
	}
	
	public Optional<Assignment> findById(String id){
		return asRepo.findById(id);
	}
	
	// INSERT methods
	public Assignment save(Assignment assignment) {
		return asRepo.save(assignment);
	}
	
	public List<Assignment> saveAll(List<Assignment> assignments){
		return asRepo.saveAll(assignments);
	}
	
	// DELETE methods
	public void delete(Assignment assignment) {
		asRepo.delete(assignment);
	}
	
	public void deleteById(String id) {
		asRepo.deleteById(id);
	}
	
	public void deleteAll() {
		asRepo.deleteAll();
	}
	
	// MISC methods
	
	public List<Assignment> assignTasks() {
		List<AssignmentModel> asModList = new ArrayList<AssignmentModel>();
		
		List<Team> teamList = teamService.findAll();
		List<TeamSkill> tsList = tsService.findAll();
		List<Task> taskList = taskService.findAll();
		
		for(Team team : teamList) {
			AssignmentModel asMod = new AssignmentModel();
			asMod.setTeamId(team.getTeamId());
			List<String> tempSkills = new ArrayList<>();
			
			for(TeamSkill ts : tsList) {
				if(asMod.getTeamId().equals(ts.getTeamId())) {
					tempSkills.add(ts.getSkill());
				}
			}
			
			asMod.setSkills(tempSkills);
			
			asModList.add(asMod);
		}
		
		int taskSize = 0; // minimum list size for task distribution
		
		for(Task task : taskList) {
			boolean taskGiven = false;
			AssignmentModel temp = new AssignmentModel();
			for(AssignmentModel asMod : asModList) {
				if(asMod.getSkills().contains(task.getSkill())) {
					if(asMod.getTasks().size() <= taskSize) {
						asMod.getTasks().add(task.getTaskId());
						taskGiven = true;
						break;
					}
					else {
						if(asMod.getTasks().size() < temp.getTasks().size()){
							temp = asMod;
						}
					}
				}
			}
			
			boolean bigger = true;
			for(AssignmentModel asMod : asModList) {
				// checks if the size of all of the AssignmentModel lists are smaller than the minimum list size
				if(asMod.getTasks().size() <= taskSize) {
					bigger = false;
				}
			}
			
			if(!taskGiven) {
				temp.getTasks().add(task.getTaskId());
				// implemented because unsure if the 'temp' is a reference or a value
				for(AssignmentModel asMod : asModList) {
					if(asMod.getTeamId().equals(temp.getTeamId())) {
						System.out.println(asMod.getTasks().size() + " " + temp.getTasks().size());
						asMod = temp;
					}
				}
			}
			
			if(bigger) {
				// increases minimum task list size if current task list minimum is higher than given minimum
				System.out.println(taskSize);
				taskSize++;
			}
		}
		
		List<Assignment> saveList = new ArrayList<>();
		for(AssignmentModel asMod : asModList) {
			Assignment save = new Assignment(asMod.getTeamId(),
					listToString(asMod.getSkills()),
					listToString(asMod.getTasks()));
			saveList.add(save);
		}
		
		saveAll(saveList);
		
		return saveList;
	}
	
	public String listToString(List<String> stringList) {
		if(stringList.size() == 0) {
			return "";
		}
		
		String str = stringList.get(0);
		
		for(int i = 1; i < stringList.size(); i++) {
			str = str + ", " + stringList.get(i);
		}
		
		return str;
	}
}
