package com.assignment.JavaAssignment.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.JavaAssignment.entity.Team;
import com.assignment.JavaAssignment.repository.TeamRepository;

@Service
public class TeamService {
	
	private TeamRepository teamRepo;
	
	@Autowired
	public TeamService(TeamRepository teamRepo) {
		this.teamRepo = teamRepo;
	}
	
	// GET methods
	public List<Team> findAll(){
		return teamRepo.findAll();
	}
	
	public Optional<Team> findById(String id){
		return teamRepo.findById(id);
	}
	
	// INSERT methods
	public Team save(Team team) {
		return teamRepo.save(team);
	}
	
	public List<Team> saveAll(List<Team> teams){
		return teamRepo.saveAll(teams);
	}
	
	// DELETE methods
	public void delete(Team team) {
		teamRepo.delete(team);
	}
	
	public void deleteById(String id) {
		teamRepo.deleteById(id);
	}
	
	public void deleteAll() {
		teamRepo.deleteAll();
	}
}
